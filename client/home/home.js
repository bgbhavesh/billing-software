
// counter starts at 0
Session.setDefault('counter', 0);

Template.home.helpers({
    counter: function () {
      return Session.get('counter');
    },
    height: function () {
        // console.log(app.RowsCalls)
        var height = $(window).height();
        
      return height;
    },
    width: function () {
        // console.log(app.RowsCalls)
        var width = $(window).width();
        
      return width;
    },
});

Template.home.events({
    'click button': function () {
      // increment the counter when button is clicked
      Session.set('counter', Session.get('counter') + 1);
    }
});

// Meteor.startup(function(){
//   $("body").append(' <script type="application/dart" src="/example.dart"></script> <script src="/dart.js"></script>');
// })