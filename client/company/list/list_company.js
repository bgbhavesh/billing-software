Template.list_company.helpers({
    height: function () {
        // console.log(app.RowsCalls)
        var height = $(window).height();        
        return height;
    },
    width: function () {
        // console.log(app.RowsCalls)
        var width = $(window).width();        
        return width;
    },
    'compamies':function(){
        return Companies.find()
    },

});
Template.list_company.events({
    'click .list_company .Ok':function(event,tpl){
        console.log(tpl)
        var button = $(event.currentTarget).find("span").removeClass("glyphicon-ok");
        var button = $(event.currentTarget).find("span").addClass("glyphicon-pencil");
        // var target = $(event.currentTarget).parent().parent().parent().find('p');
        // console.log(target)
        $("p").attr("contenteditable","false");
        // console.log($(target).text())
        var button = $(event.currentTarget).removeClass("Ok");
        var button = $(event.currentTarget).addClass("Edit");
        //createAttribute
        app.updateValue(this._id);
    },
    'click .list_company .Edit':function(event,tpl){
        console.log(tpl)
        var button = $(event.currentTarget).find("span").removeClass("glyphicon-pencil");
        var button = $(event.currentTarget).find("span").addClass("glyphicon-ok");
        var target = $(event.currentTarget).parent().find('p');
        console.log(target)
        // console.log($(target).text())
        
        $(target).attr("contenteditable","true");
        var button = $(event.currentTarget).removeClass("Edit");
        var button = $(event.currentTarget).addClass("Ok");
        //createAttribute
    },
    'click .list_company .Delete':function(tpl){
        Companies.remove(this._id);
    },
    "dblclick .list_company td":function(){

    }
    

})
app.updateValue = function(id){
    var companyName = $("#companyName").text();
    var registerationno = $("#registerationno").text();
    var postalcode = $("#postalcode").text();
    var slogan = $("#slogan").text();
    var panno = $("#panno").text();
    var vatno = $("#vatno").text();
    var servicetaxno = $("#servicetaxno").text();
    var CompanyLogo = $("#CompanyLogo").text();
    var cityName = $("#cityName").text();
    var stateName = $("#stateName").text();
    var countryName = $("#countryName").text();
    var contactName = $("#contactName").text();
    var contactDesignation = $("#contactDesignation").text();
    var company_email = $("#company_email").text();
    var createrMessage = $("#createrMessage").text();
    var address = $("#address").text();

    var update =
    {
        "companyName":companyName,
        "registerationno":registerationno,
        "postalcode":postalcode,
        "slogan":slogan,
        "panno":panno,
        "vatno":vatno,
        "servicetaxno":servicetaxno,
        "CompanyLogo":CompanyLogo,
        "cityName":cityName,
        "stateName":stateName,
        "countryName":countryName,
        "contactName":contactName,
        "contactDesignation":contactDesignation,
        "company_email":company_email,
        "createrMessage":createrMessage,
        "address":address,
    }
    Companies.update({"_id":id},{$set:update});

    // $("#companyName").text(companyName);
    // $("#registerationno").text(registerationno);
    // $("#postalcode").text(postalcode);
    // $("#slogan").text(slogan);
    // $("#panno").text(panno);
    // $("#vatno").text(vatno);
    // $("#servicetaxno").text(servicetaxno);
    // $("#CompanyLogo").text(CompanyLogo);
    // $("#cityName").text(cityName);
    // $("#stateName").text(stateName);
    // $("#countryName").text(countryName);
    // $("#contactName").text(contactName);
    // $("#contactDesignation").text(contactDesignation);
    // $("#company_email").text(company_email);
    // $("#createrMessage").text(createrMessage);
    // $("#address").text(address);

}