
Template.add_company.helpers({

    height: function () {
        // console.log(app.RowsCalls)
        var height = $(window).height();
        
      return height;
    },
    width: function () {
        // console.log(app.RowsCalls)
        var width = $(window).width();
        
      return width;
    },
});
Template.add_company.events({
    'click button.submit':function(event,tpl){
        if(!app.add_company_validataion())
            return;
    }
})
app.add_company_validataion = function(){
    var companyName = $('.add_company #companyName').val();
    if(companyName == '' || companyName == null)
    {        
        $('.add_company .error_message').css("display","block");
        $('.add_company .error_message h5').text("Missisng Company Name");
        return false;
    }
    var address = $('.add_company #address').val();
    if(address == '' || address == null)
    {        
        $('.add_company .error_message').css("display","block");
        $('.add_company .error_message h5').text("Missisng Company address");
        return false;
    }
    var postalcode = $('.add_company #postalcode').val();
    if(postalcode == '' || postalcode == null)
    {        
        $('.add_company .error_message').css("display","block");
        $('.add_company .error_message h5').text("Missisng Company postal code");
        return false;        
    }

    // companyName
    // address
    // postalcode
    var slogan = $('.add_company #slogan').val();
    var panno = $('.add_company #panno').val();
    var registerationno = $('.add_company #registerationno').val();
    var vatno = $('.add_company #vatno').val();
    var servicetaxno = $('.add_company #servicetaxno').val();
    var CompanyLogo = $('.add_company #CompanyLogo').val()
    var cityName = $('.add_company #cityName').val()
    var stateName = $('.add_company #stateName').val()
    var countryName = $('.add_company #countryName').val()
    var contactName = $('.add_company #contactName').val()
    var contactDesignation = $('.add_company #contactDesignation').val()
    var company_email = $('.add_company #company_email').val()
    var createrMessage = $('.add_company #createrMessage').val()

    $('.add_company #slogan').val("");
    $('.add_company #panno').val("");
    $('.add_company #postalcode').val("");
    $('.add_company #companyName').val("");
    $('.add_company #registerationno').val("");
    $('.add_company #vatno').val("");
    $('.add_company #address').val("");
    $('.add_company #servicetaxno').val("");
    $('.add_company #CompanyLogo').val("");
    $('.add_company #cityName').val("");
    $('.add_company #stateName').val("");
    $('.add_company #countryName').val("");
    $('.add_company #contactName').val("");
    $('.add_company #contactDesignation').val("");
    $('.add_company #company_email').val("");
    $('.add_company #createrMessage').val("");

    Companies.insert({companyName: companyName,address:address,createdAt:Date(),registerationno:registerationno,postalcode:postalcode,slogan:slogan,panno:panno,vatno:vatno,servicetaxno:servicetaxno,CompanyLogo:CompanyLogo,cityName:cityName,stateName:stateName,countryName:countryName,contactName:contactName,contactDesignation:contactDesignation, company_email:company_email,createrMessage:createrMessage});
    return true;
}