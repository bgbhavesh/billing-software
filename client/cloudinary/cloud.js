app.cloudinary = {};
app.cloudinary.cloud_name = "nicolsondsouza";
app.cloudinary.api_key = "239412182698181";
app.cloudinary.unsigned = "znwytvn2";
$.cloudinary.config({cloud_name: app.cloudinary.cloud_name, api_key: app.cloudinary.api_key});

app.cloudinary.imageUpload = function(){
    $('#dog-profile-upload').unsigned_cloudinary_upload(app.cloudinary.unsigned, 
        { cloud_name: 'nicolsondsouza', tags: 'CompanyLogo' }, 
        { multiple: false }
    ).bind('cloudinarydone', function(e, data) {
        var image = data.result.secure_url;
        // console.log(image);
        var middle = "w_50,h_50,c_fill";
        var new_img_url = null;
        var img_url = image
        new_img_url = img_url.replace("image/fetch/","image/fetch/"+middle+"/");
        img_url = img_url.split("upload/")
        var first = img_url[0] +"upload/";
        var second = "/" +img_url[1].split("/")[1];
        new_img_url = first +middle + second;

        Meteor.users.update({"_id":Meteor.userId()},{$set:{"profile.dogPicture":new_img_url}});
        $("#uploadProgress").css("opacity",1.0).html("100%");
        app.cloudinary.hideProgress();
    }).bind('cloudinaryprogress', function(e, data) { 
        var progress = Math.round((data.loaded * 100.0) / data.total) + '%';
        $("#uploadProgress").css("opacity",1.0).html(progress);
    }).bind("fileuploadfail",function(e,data){
        // console.log("fileuploadfail");
        $("#uploadProgress").css("opacity",1.0).html("!");
        app.cloudinary.hideProgress();
    });
}

app.cloudinary.hideProgress = function(){
    setTimeout(function(){
        $("#uploadProgress").css("opacity",0.0).html("");
    },100)
}