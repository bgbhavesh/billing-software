Router.map(function () {
    this.route("home",{
        path:"/",
        // loadingTemplate: 'loading',
        template:"basicLayout",
        yieldTemplates:
            {
        //         // 'landingHeader': {to: 'header'},
                'leftDiv': {to: 'slider'},
                'home': {to: 'home'},
            },                
    });
    this.route("create_invoice",{
        path:"/create_invoice",
        // loadingTemplate: 'loading',
        template:"basicLayout",
        yieldTemplates:
            {
        //         // 'landingHeader': {to: 'header'},
                'leftDiv': {to: 'slider'},
                'createInvoice': {to: 'home'},
            },                
    });
    this.route("list_invoice",{
        path:"/list_invoice",
        // loadingTemplate: 'loading',
        template:"basicLayout",
        yieldTemplates:
            {
                'leftDiv': {to: 'slider'},
                'listInvoice': {to: 'home'},
            },                
    });

    this.route("view_invoice",{
        path:"/view_invoice/:_id",
        // loadingTemplate: 'loading',
        template:"basicLayout",
        yieldTemplates:
            {
                'leftDiv': {to: 'slider'},
                'viewInvoice': {to: 'home'},
            },                
    });
    this.route("add_item",{
        path:"/add_item",
        // loadingTemplate: 'loading',
        template:"basicLayout",
        yieldTemplates:
            {
                'leftDiv': {to: 'slider'},
                'createItem': {to: 'home'},
            },                
    });
    this.route("list_item",{
        path:"/list_item",
        // loadingTemplate: 'loading',
        template:"basicLayout",
        yieldTemplates:
            {
                'leftDiv': {to: 'slider'},
                'listItem': {to: 'home'},
            },                
    });
    this.route("view_item",{
        path:"/view_item/:_id",
        // loadingTemplate: 'loading',
        template:"basicLayout",
        yieldTemplates:
            {
                'leftDiv': {to: 'slider'},
                'viewItem': {to: 'home'},
            },                
    });
    this.route("list_company",{
        path:"/list_company",
        // loadingTemplate: 'loading',
        template:"basicLayout",
        yieldTemplates:
            {
                'leftDiv': {to: 'slider'},
                'list_company': {to: 'home'},
            },        
        // onBeforeAction: function(pause){
        //     if (Meteor.user()){
        //         Router.go('tweet');
        //     }
        //     else{
        //         this.next()
        //     }            
        
    });
    this.route("add_company",{
        path:"/add_company",
        // loadingTemplate: 'loading',
        template:"basicLayout",
        yieldTemplates:
            {
                'leftDiv': {to: 'slider'},
                'add_company': {to: 'home'},
            },        
        // onBeforeAction: function(pause){
        //     if (Meteor.user()){
        //         Router.go('tweet');
        //     }
        //     else{
        //         this.next()
        //     }            
        
    });
    this.route("view_company",{
        path:"/view_company/:_id",
        // loadingTemplate: 'loading',
        template:"basicLayout",
        yieldTemplates:
            {
                'leftDiv': {to: 'slider'},
                'viewCompany': {to: 'home'},
            },        
        // onBeforeAction: function(pause){
        //     if (Meteor.user()){
        //         Router.go('tweet');
        //     }
        //     else{
        //         this.next()
        //     }            
        
    });
    this.route("login",{
        path:"/login",
        // loadingTemplate: 'loading',
        template:"basicLayout",
        yieldTemplates:
            {
                'leftDiv': {to: 'slider'},
                'login': {to: 'home'},
            },        
        // onBeforeAction: function(pause){
        //     if (Meteor.user()){
        //         Router.go('tweet');
        //     }
        //     else{
        //         this.next()
        //     }            
        
    });

    this.route("signup",{
        path:"/signup",
        // loadingTemplate: 'loading',
        template:"basicLayout",
        yieldTemplates:
            {
                'leftDiv': {to: 'slider'},
                'signup': {to: 'home'},
            },        
        // onBeforeAction: function(pause){
        //     if (Meteor.user()){
        //         Router.go('tweet');
        //     }
        //     else{
        //         this.next()
        //     }            
        
    });
    this.route("listInvoice",{
        path:"/listInvoice",
        // loadingTemplate: 'loading',
        template:"basicLayout",
        yieldTemplates:
            {
                'leftDiv': {to: 'slider'},
                'listInvoice': {to: 'home'},
            },        
        // onBeforeAction: function(pause){
        //     if (Meteor.user()){
        //         Router.go('tweet');
        //     }
        //     else{
        //         this.next()
        //     }            
        
    });
    this.route("leftDiv",{
        path:"/leftDiv",
        // loadingTemplate: 'loading',
        template:"leftDiv",
        yieldTemplates:
            {
                // 'slider': {to: 'slider'},
                'leftDiv': {to: 'home'},
            },        
        // onBeforeAction: function(pause){
        //     if (Meteor.user()){
        //         Router.go('tweet');
        //     }
        //     else{
        //         this.next()
        //     }            
        
    });
      this.route("add_customer",{
        path:"/add_customer",
        // loadingTemplate: 'loading',
        template:"basicLayout",
        yieldTemplates:
            {
                'leftDiv': {to: 'slider'},
                'add_customer': {to: 'home'},
            },        
        // onBeforeAction: function(pause){
        //     if (Meteor.user()){
        //         Router.go('tweet');
        //     }
        //     else{
        //         this.next()
        //     }            
        
    });
});